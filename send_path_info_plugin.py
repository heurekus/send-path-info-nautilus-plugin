'''
 Send Path Info Plugin for the Nautilus File Manager
 
 Hooks into the context menu when a directory or a file is selected
 and shows an entry to send a message. When selected, a message text 
 can be entered by the user which is sent alongside the selected
 path (+filename). The message is sent as an HTTP request.
 
 Parameters can be configured in ~/.send_path_info_plugin.cfg
  

 @version    see below
 @package    send_path_info_plugin
 @copyright  Copyright (c) 2020 Martin Sauter
 @license    GNU General Public License v3
 @since      Since Release 1.0.0

 
 Tested on:
 
   Ubuntu 18.04
   Ubuntu 20.04 (Nemo version only)
 
 Installation / Debugging, etc. 
 
 See README.md
 
 Python Version:
   
 * Even Ubuntu 20.04 still uses the Python 2.7 environment for 
   nautilus-python extensions. As pip for python2 is NOT installed in 
   Ubuntu 20.04 it is not possible to add any other Python packages. One
   can only work with those present by default in /usr/lib/python2.7 

'''

VERSION = "1.0.9"

from gi.repository import Nautilus, GObject
import ConfigParser
import urllib
import requests
import os 
import logging
from gi.repository import Gtk
from datetime import datetime
import syslog
from __builtin__ import False

DATE_FORMAT = '%d.%m.%Y, %H:%M:%S'

# Read the config file from the user's home folder    
config_filename = os.path.expanduser("~") + "/.send_path_info_plugin.cfg"
log_filename = os.path.expanduser("~") + "/send_path_info_plugin.log"

# Uncomment the command below to log activity to a file
# Note: path must be absolute, it can't start at '~/...'  !!!
#logging.basicConfig(filename=log_filename,level=logging.INFO, \
#                    format='%(asctime)s %(message)s')


class MessageSendError(Exception):
    pass

class ConfigFileError(Exception):
    pass        


class SendPathInfo(GObject.GObject, Nautilus.MenuProvider):
    
    context_menu_text = ""
    
    def __init__(self):

        logging.info('SendPI: ')
        logging.info('SendPI: ')
        logging.info('SendPI: Nautilus Send Path Info add-on installed, version: ' + VERSION)
        
        try:
            self.context_menu_text = get_param_from_config('Settings', 'Context_Menu_Text')
        except Exception as e:
            logging.info('SendPI: Config File ERROR: Unable to get string for context menu ' 
                         + str(e))
            self.context_menu_text = 'Send Info about File/Directory'            

    def menu_activate_cb(self, menu, file):

        logging.info('SendPI: Menu entry selected')        

        if file.is_gone():
            return

        dir_name = urllib.unquote(file.get_uri())[7:]
        logging.info('SendPI: Path and filename : ' + str(dir_name))
        
        GatherInfoWindow = SendPathInfoGtkInfoWindow(dir_name)
        GatherInfoWindow.execute()        
        

    def get_file_items(self, window, files):

         # Only single selections allowed
        if len(files) != 1:
            return

        file = files[0]       

        # Get the directory name and remove the URI part (file://)
        dir_name = urllib.unquote(file.get_uri())[7:]
 
        item = Nautilus.MenuItem(name = 'Nautilus::send_path_info',
                                 label = self.context_menu_text,
                                 tip = '-')
        item.connect('activate', self.menu_activate_cb, file)
        return item,


class SendPathInfoGtkInfoWindow():
    """
    Main GUI class. Receives event when the context menu entry for
    this add-on is selected. It then presents a window for the user to
    type in text. With information in the config file an HTTP POST 
    message is then assembled and sent. 
    """
    
    # UI components
    win = None
    grid = None
    entry = None
    emptyLabel = None
    
    sendButton = None
    cancelButton = None
    
    def __init__(self, full_path):
    
        logging.info('SendPI: User Interface Initialization')
    
        # Copy the given path to an object variable for later use
        self.dir_and_filename = full_path
    
        # Create the info window
        self.win = Gtk.Window(title='Send Path Info')
        
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(6)
        self.grid.set_column_spacing(6)
        self.grid.set_margin_top(10)
        self.grid.set_margin_bottom(10)
        self.grid.set_margin_left(15)
        self.grid.set_margin_right(15)
        self.grid.set_column_homogeneous(False)
        self.win.add(self.grid)

        self.entry = Gtk.Entry()
        self.entry.set_text("")
        self.grid.attach(self.entry, 1, 1, 3, 1)
        
        self.cancelButton = Gtk.Button(label="Cancel")
        self.cancelButton.connect('clicked', self.__cancel_bt_clicked)
        self.grid.attach_next_to(self.cancelButton, self.entry, Gtk.PositionType.BOTTOM, 1, 1)
        
        self.emptyLabel = Gtk.Label(label="                                               " + 
                                    "                                  ")
        self.grid.attach_next_to(self.emptyLabel, self.cancelButton, Gtk.PositionType.RIGHT, 1, 1)
         
        self.sendButton = Gtk.Button(label="   Send   ")
        self.sendButton.connect("clicked", self.__run_send_bt_clicked)        
        self.grid.attach_next_to(self.sendButton, self.emptyLabel, Gtk.PositionType.RIGHT, 1, 1)
        
    
    def execute(self):
        """
        This is the main method that shows the window created in the class
        initialization function above and returns immediately. The user
        can then select the actions to be taken. 
        """
        self.win.connect("destroy", Gtk.main_quit)
        self.win.show_all()
                
        # Main UI loop. It is left when the window is closed in example_target()        
        Gtk.main()    

    
    def __cancel_bt_clicked(self, button):
        self.win.close()

    
    def __run_send_bt_clicked(self, button):

        try:
            push_URL = get_param_from_config('Settings', 'Push_URL')

            if push_URL == "" : 
                raise MessageSendError("Push_URL empty in config file, can't send message")
            
            if not push_URL.startswith(('http', 'HTTPS')):
                raise MessageSendError("Push_URL parameter in config file must start " + 
                                       "with http or https")
            
            user_text = self.entry.get_text()
            if user_text == "": user_text = "---"
    
            logging.info('SendPI: -----------------')        
            logging.info('SendPI: Path: ' + self.dir_and_filename)
            logging.info('SendPI: Text: ' + user_text)
            logging.info('SendPI: URL: ' + push_URL)
            
            user_message = (user_text + "\n" + 
                            self.dir_and_filename  + "\n" + 
                            datetime.now().strftime(DATE_FORMAT))
            
            http_post_params = create_dict_from_config_file_section('HTTP_Post_Parameters',
                                                                    user_message)
            if http_post_params == False:
                raise MessageSendError('ERROR: reading HTTP Post Parameter from ' + 
                                       'config failed')

            # Now trigger the HTTP post            
            resp = requests.post(push_URL, data=http_post_params, timeout=4)
        
            if resp.status_code != 200: 
                raise MessageSendError('Error sending push message, HTTP code: ' + 
                                       str(resp.status_code))
                
        except Exception as e:
            syslog.syslog('SendPI: requests.post failed, message not sent: ' + str(e))
            logging.info('SendPI: requests.post failed, message not sent: ' + str(e))            
            self.__show_error_dialog_box_and_exit(str(e))
            return

        # Success, the main send path info window can be closed
        syslog.syslog('SendPI: Message sent successfully')
        logging.info('SendPI: Message sent successfully')
        self.win.close()


    def __show_error_dialog_box_and_exit(self, error_text):
        
        dialog = Gtk.MessageDialog(flags=0,message_type=Gtk.MessageType.ERROR,
                                   buttons=Gtk.ButtonsType.OK,
                                   text="Failed to send message",)
        dialog.format_secondary_text(error_text)
        dialog.run()
        dialog.destroy()
        # Close the main sent path info window as well
        self.win.close() 
       
#
# Config File Read Functions for all classes
#
 
def get_param_from_config(section_name, param_name):

    """
    For the config file of this project get a parameter from the
    section given as the two parameters to the function.
    
    Will throw an EXCEPTION if the config file or the parameter 
    is not present. Therefore this function needs to be encapsulated 
    in a 'try' block!
    """
    
    content = ""
   
    config = ConfigParser.ConfigParser()
    config.sections()       
    config.read(config_filename)
    
    if not config.sections():
        raise ConfigFileError('Config File ERROR: Unable to open config ' +
                              'file: ' + config_filename)

    try:
        content = config.get(section_name, param_name)
    except Exception as e:
        raise ConfigFileError('Config File ERROR: Unable to find parameter ' + 
                              param_name + ' in section: ' + section_name)
        
    return content 


def create_dict_from_config_file_section(section_name, msg_text):
    """
    This function reads a complete section of the config file and puts
    the result in a dictionary. In addition it will search the dictionary
    for a name/value combination in which value = 'MSG'. If found
    it will replace 'MSG' with msg_text.
    
    Note: The dictionary that is returned can for example hold the 
    http post parameters for Python's request.post() function 

    Will throw an EXCEPTION if there is a problem during dictionary
    generation.
    
    Returns: The dictionary with name/values of all parameters of the
    given section. Returns FALSE if there was a problem generating the
    dictionary.
    """
    
    http_post_vars = False
    
    # Read all sections of the config file. This is done every time a message
    # is to be assembled so config file changes can be used without restarting
    # Nautilus.
    config = ConfigParser.ConfigParser()
    config.sections()       
    config.read(config_filename)    
    
    if not config.sections():
        raise ConfigFileError('Config File ERROR: Unable to open configuration file: ' + 
              config_filename)
    
    # Covert Post_Parameters section into a dictionary that can be used by 
    # Python's request.post() function as post parameters
    try:
        http_post_vars = dict(config.items(section_name))
    except Exception as e:
        raise ConfigFileError('Config File ERROR: Section or parameter missing: ' 
                              + str(e))
    
    # Now replace MSG with the message text given to the function
    try:
        for var in http_post_vars:
            # If the current post variable contains the magic string "MSG"
            # replace it with the message text given to the function     
            if http_post_vars[var] == "MSG":
                http_post_vars[var] = msg_text
    
    except Exception as e:
        raise ConfigFileError ('Config File ERROR: Unable to process the ' + 
                               'section parameters: ' + str(e))
    
    return http_post_vars        
         
    