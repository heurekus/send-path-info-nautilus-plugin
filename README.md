# Send Path Info - Nautilus Plugin (Nemo supported as well)

Hooks into the context menu when a directory or a file is selected and shows an entry to send a message. When selected, a message text can be entered by the user which is sent alongside the selected path (+filename). The message is sent as an HTTP POST request. The service that receives the message and how the URL + HTTP post variables look like can be configured in the configuration file of the add-on. [Gotify](https://gotify.net/) is an example of a message server that can be used for this puprose.
 
 ## Screenshots

 ![](images/send-path-info.png)

 ## OS Versions tested

* Ubuntu 18.04

* Ubuntu 20.04 (Nemo version only)

* Ubuntu 22.04 (Nemo version only)

## Installation

* Install the following packages: 
 
```
sudo apt install python-nautilus
```

* Create the following path and copy 'send_path_info_plugin.py' into it: 

```
mkdir ~/.local/share/nautilus-python/
mkdir ~/.local/share/nautilus-python/extensions
cp send_path_info_plugin.py ~/.local/share/nautilus-python/extensions
```

* Copy the config file to the HOME folder:

```
cp .send_path_info_plugin.cfg ~
```

* Edit the configuration file (Add push URL, parameters, etc.)

* Restart Nautilus: 
 
```
sudo killall nautilus
```

## Nemo - Required Changes

Nemo is a fork of Nautilus used in some Linux distributions. It can also be used as an alternative filemanger in Ubuntu. The following changes are required to make this plugin work in Nemo:

* Use apt to install all non-Nautilus extensions described above.

* Install nemo python support:

```
sudo apt install nemo-python
```

* Perform the following changes in the file. SED is your friend:

```
cp send_path_info_plugin.py send_path_info_plugin-FOR-NEMO.py 

sed -i 's/Nautilus/Nemo/g' send_path_info_plugin-FOR-NEMO.py
sed -i 's/= ConfigParser/= configparser/g' send_path_info_plugin-FOR-NEMO.py
sed -i 's/import ConfigParser/import configparser/g' send_path_info_plugin-FOR-NEMO.py
sed -i 's/urllib.unquote/urllib.parse.unquote/g' send_path_info_plugin-FOR-NEMO.py
sed -i 's/urllib.quote/urllib.parse.quote/g' send_path_info_plugin-FOR-NEMO.py
sed -i 's/from __builtin__ import False/ /g' send_path_info_plugin-FOR-NEMO.py
```

* Copy 'send_path_info_plugin-FOR-NEMO.py' to the following directory:

```
/usr/share/nemo-python/extensions
```

* Copy config file to home directory and edit parameters as described above


## Debugging / Logging


## Logging

* Enable logging to a file in send_path_info_plugin.py

```
# Uncomment the command below to log activity to a file
# Note: path must be absolute, it can't start at '~/...'  !!!
#logging.basicConfig(filename=log_filename,level=logging.INFO, \
#                    format='%(asctime)s %(message)s')
```

* Then end all Nautilus tasks

```
sudo killall nautilus
```

 * Run nautilus from this directory as follows:

```
nautilus --no-desktop
```

 * The log file will then be created in the directory from which nautilus was started.
 * Things work when the shell does NOT return to the command line while this Nautilus instance is running
 * In a second shell monitor the log file. First, wait until it is created then do a 'tail' on it:

```
tail -f ~/send_path_info_plugin.log
```

## Python Version:
   
Even Ubuntu 20.04 still uses the Python 2.7 environment for nautilus-python extensions. As pip for python2 is NOT installed in Ubuntu 20.04 it is not possible to add any other Python packages. One can only work with those present by default in /usr/lib/python2.7
